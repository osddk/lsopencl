// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

// Compile: g++ -Wall -o opencltest opencltest.cpp -lOpenCL

#include <CL/opencl.h>

#include <string.h>
#include <stdio.h>

void dumpPlatformInfo(cl_platform_id platform, cl_platform_info param_name) {
  cl_int res;

  size_t buf_size = 512;
  char* buf = new char[buf_size];
  res = clGetPlatformInfo(platform, param_name, buf_size, buf, NULL);
  switch (res) {
    case CL_SUCCESS:
      printf("%s\n", buf);
      break;
    case CL_INVALID_PLATFORM:
      printf("Error: Invalid platform\n");
      break;
    case CL_INVALID_VALUE:
      printf("Error: Platform query param not supported or response "
             "buffer too small\n");
      break;
    default:
      printf("Error: Unknown error\n");
      break;
  }
  delete[] buf;
}

void dumpDeviceInfo(cl_device_id device, cl_device_info param_name) {
  cl_int res;

  size_t buf_size = 512;
  char* buf = new char[buf_size];
  res = clGetDeviceInfo(device, param_name, buf_size, buf, NULL);
  switch (res) {
    case CL_SUCCESS:
      printf("%s\n", buf);
      break;
    case CL_INVALID_DEVICE:
      printf("Error: Invalid device\n");
      break;
    case CL_INVALID_VALUE:
      printf("Error: Device query param not supported or response buffer "
             "too small\n");
      break;
    default:
      printf("Error: Unknown error\n");
      break;
  }
  delete[] buf;
}

int main() {
  cl_uint num_platforms;
  cl_int res;

  // Get number of available platforms.
  res = clGetPlatformIDs(0, NULL, &num_platforms);
  if (res != CL_SUCCESS) {
    printf("Error: Failed getting platforms\n");
    return 1;
  }
  printf("Platforms available: %d\n", num_platforms);
  if (num_platforms == 0) {
    return 0;
  }

  cl_platform_id* platforms = new cl_platform_id[num_platforms];
  res = clGetPlatformIDs(num_platforms, platforms, NULL);
  for (uint p = 0; p < num_platforms; p++) {
    printf("\nPlatform %d Info:\n", p);

    printf("Vendor: ");
    dumpPlatformInfo(platforms[p], CL_PLATFORM_VENDOR);

    printf("Name: ");
    dumpPlatformInfo(platforms[p], CL_PLATFORM_NAME);

    printf("Version: ");
    dumpPlatformInfo(platforms[p], CL_PLATFORM_VERSION);

    printf("Extensions: ");
    dumpPlatformInfo(platforms[p], CL_PLATFORM_EXTENSIONS);

    cl_uint num_devices;
    res = clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL, 0, NULL,
                         &num_devices);
    if (res != CL_SUCCESS) {
      printf("Error: Failed getting list of devices\n");
      delete[] platforms;
      return 1;
    }
    printf("\nPlatform %d devices available: %d\n", p, num_devices);

    cl_device_id* devices = new cl_device_id[num_devices];
    res = clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL, num_devices,
                         devices, NULL);
    if (res != CL_SUCCESS) {
      printf("Error: Failed getting devices\n");
      delete[] platforms;
      return 1;
    }

    for (uint d = 0; d < num_devices; d++) {
      printf("\nPlatform %d Device %d:\n", p, d);

      printf("Vendor: ");
      dumpDeviceInfo(devices[d], CL_DEVICE_VENDOR);

      printf("Name: ");
      dumpDeviceInfo(devices[d], CL_DEVICE_NAME);

      printf("Version: ");
      dumpDeviceInfo(devices[d], CL_DEVICE_VERSION);
    }

    delete[] devices;
  }

  delete[] platforms;
  return 0;
}
