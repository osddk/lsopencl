lsopencl
=======

Introduction
------------

Simple concept tool that lists available OpenCL platforms and devices.


Example
-------

Running lsopencl results in output like this:

    Platforms available: 1

    Platform 0 Info:
    Vendor: NVIDIA Corporation
    Name: NVIDIA CUDA
    Version: OpenCL 1.1 CUDA 6.5.11
    Extensions: cl_khr_byte_addressable_store cl_khr_icd cl_khr_gl_sharing cl_nv_compiler_options cl_nv_device_attribute_query cl_nv_pragma_unroll

    Platform 0 devices available: 2

    Platform 0 Device 0:
    Vendor: NVIDIA Corporation
    Name: GeForce GTX 670
    Version: OpenCL 1.1 CUDA

    Platform 0 Device 1:
    Vendor: NVIDIA Corporation
    Name: GeForce GTX 670
    Version: OpenCL 1.1 CUDA


Building
--------

You'll need cmake and opencl installed to build this. To install the necessary tools on Ubuntu:

    apt-get install build-essential cmake ocl-icd-opencl-dev

To build the tool run:

    cmake .
    make

You should now be able to run the lsopencl tool:

    ./lsopencl
